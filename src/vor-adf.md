VOR

* Input VOR frequency into NAV1
* Press NAV1 on com to listen and identify
* Enter current track and observe needle positions correctly

ADF

* Input ADF frequency
* Press ADF button on com to identify
* Press ADF button on ADF to test needle moves, then OFF to reposition needle
