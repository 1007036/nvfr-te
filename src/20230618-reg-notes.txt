Brief
  Human Factors for night flight
  Night circuits
Short Brief
  VOR

Pre-test route
  YBAF - Crows Nest - YDAY - YBOK - YTWB - YBAF
    * YBOK VOR
    * Circuits at YDAY

Test route
  YBAF - BML - YWCK - YTWB - YBAF
    * divert: no YTWB

TODO
  ~~Briefings~~
  NAV plan
  ~~LMS https://act.talentlms.com/~~
  ~~Training record~~
    ~~check~~
    ~~MAAT syllabus~~
    ~~Labels~~
  Study MET
  Part 61 MOS on NVFR Rating

Briefings 2023-09-01

* GNSS
* VOR
* ADF/NDB
* Night Circuits
* Night Navigation
* Human Factors for NVFR

2023-09-01

TEM (solo assessment)
Conscious (of the environment SA, conflict identification)
Communicate (talk on the radio?)
Cautious
Control (power/attitude -> performance)
Confident
Consistent
Corrections (awareness, prevention, recovery)

ADF slides: "TRack 300 to station"
  Mistake in one of hte ADF indications in the track intercepts
  Threat+Error -> Undesired Aircraft State
    How am I going to prevent it?
