# Night VFR Training Progress

#### 2023-05-19

* Aircraft: VH-OCP Cessna 172S
* Instructor: Steve Taddeucci
* Sequence: Night VFR Circuits TE9-12, TE9-14
* 1.2/0.8

#### 2023-05-23

* Aircraft: VH-OCP Cessna 172S
* Instructor: Steve Taddeucci
* Sequence: YBAF-YBSU-YKRY-YBOK-YBAF TE9-19, TE9-20
* 3.0/2.8
