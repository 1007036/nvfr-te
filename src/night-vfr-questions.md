# Night VFR Questions

1. **What are the requirements for the grant of NVFR endorsement and rating?** _(CASR Subpart 61.O - Night VFR ratings) [link](casr1998-part61-subpart61o.pdf)_

* 10 hours of aeronautical experience at night, including 5 hours dual cross-country _(CASR1998 61.975)_
* two flights, each with at least one landing at a non-departure aerodrome, remote from extensive ground lighting _(CASR1998 61.975)_
* 5 hours of aeronautical experience at night, including 1 hour dual, 1 hour solo circuit _(CASR1998 61.980)_
* 3 hours dual instrument _(CASR1998 61.980)_

2. **What are the minimum instrument requirements for NVFR?** _(Part 91 MOS 26.07) [link](part91-mos-26.07.pdf)_

* airspeed indicator
* pressure altitude
* magnetic heading
* accurate clock or watch
* turn and slip indicator
* attitude indicator
* vertical speed indicator
* stabilised heading indicator
* outside air temperature indicator
* an indication of the power supply to gyroscopic instruments

3. **What are the minimum aircraft lighting requirements for NVFR?** _(Part 91 MOS 26.7) [link](part91-mos-26.7.pdf)_

* one landing light or two for charter
* red, green and white position lights
* anti-collision light
* instrument lighting for all essential instruments
* two sources of power for instrument lighting
* pilot and passenger compartment lighting to read maps/documentation
* shock-proof torch for each crew member

4. **What are the minimum aircraft radio and navaid requirements for NVFR?** _(Part 91 MOS 26.07) [link](part91-mos-26.07.pdf)_

* VHF
* one of ADF, VOR, GNSS

5. **What are the currency requirements for NVFR?**

* 1 take-off and landing at night in the previous 6 months _(CASR1998 61.965)_
* flight review at night in the previous 24 months _(CASR1998 61.965)_
* if carrying passengers, 3 take-off and landing in the previous 90 days _(CASR1998 61.395(2))_

6. **What are the four basic areas of flight-test competency for NVFR?** _(AC 61-05 (4.7.3)) [link](ac-61-05.pdf)_

* flying solely by reference to instruments
* night take-off, circuit and landing
* navigation at night by visual reference, both with and without the use of radio navigation aids
* non-normal and emergency conditions (simulated)

7. **What are the basic requirements for NVFR Lowest Safe Altitude (LSALT)?** _(AIP GEN 3.3 (4)) [link](aip-gen-3.3-lsalt.pdf)_

* 10 NM radius of the aircraft, unless taking-off or landing, 1000ft clear of all obstacles OR using IFR LSALT on ERC Charts or AIP GEN
* Descent to a lower altitude if an in-flight visual fix has been passed and the 1000ft obstacle clearance is maintained ahead
* Descending below LSALT requires the aerodrome in sight and within 3 NM, maintaining terrain clearance

8. **When must an alternate aerodrome be planned for NVFR?** _(AIP ENR 1.1 (10.7)) [link](aip-enr-1.1-alternate-aerodromes.pdf)_

* if the aerodrome is not serviced by a VOR or NDB, or the pilot is not competent to operate the IFR-approved GNSS receiver
* runway lighting system unserviceable
* more than SCT below 1500ft
* forecast or PROB less than 8km
* forecast or PROB for TS or SEV TURB
* crosswind greater than for the aircraft
* not required if improvement forecast above minima, if sufficient fuel to hold until 30 mins after
* not required if sufficient fuel to hold during period of INTER + 30 mins, or TEMPO + 60 mins

9. **Under what conditions can a simulated engine failure be performed at night?** _(CASR 91.740) [link](casr1998-91.740.pdf)_

* for the purpose of training, checking or testing
* only crew members aboard
* PiC holds instructor/examiner rating
* commenced > 1000ft AGL
* within glide of lighted runway

----

###### TODO

* ALA runway parameters
