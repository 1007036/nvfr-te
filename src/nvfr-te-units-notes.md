# NVFR Training Endorsement Units

# Ground and Flight Training Summary (MAAT) Notes

### FIR-TE9.1 Review – Part 61 & Part 61 MOS for Night VFR rating, Flight Test & Flight Review requirements
### FIR-TE9.1 Review – Knowledge of competency based training as applied to night VFR rating training

* 1. Reserved
* 2. Flight rules
    * 2.1 Legislation
        * 2.1.1 Describe the privileges and limitations of the rating.
        * 2.1.2 Describe the minimum NVFR aircraft equipment requirements.
        * 2.1.3 Describe the ALA/HLS dimension and lighting requirements as applicable.
* 3. Flight at night
    * 3.1 Operations
        * 3.1.1 Describe the principles of operations, limitations and errors for the radio navigation systems used.
        * 3.1.2 Describe the flight planning/notification; requirements, including LSALT, weather, fuel and lighting.
        * 3.1.3 Explain the requirements for departure and descent for clearance from terrain.
        * 3.1.4 Explain the alternate aerodrome planning requirements.
        * 3.1.5 Describe the operation of PAL.
        * 3.1.6 Describe the ATC procedures relevant to NVFR operations.
    * 3.2 Human factors
        * 3.2.1 Explain the human factors and physiological limitations for the conduct of operations at night as described in CASA guidance material for NVFR operations.

* Privileges and limitations
    * 6 months, one night take-off and landing _(61.965)_
    * 24 months flight review _(61.970)_
    * PPL or higher _(61.970)_
    * 10 hours including 5 hours dual cross-country, at least 2 flights, each with one landing that is remote from extensive ground lighting _(61.975)_
    * NVFR endorsement single-engine _(61.980)_
      * at least 5 hours which is at least 1 hour dual, 1 hour solo circuits
      * at least 3 hours dual instrument

Part 61 Manual of Standards SECTION 2.7 NIGHT VFR RATING
Unit 2.7.1 NVFR: NVFR rating – all aircraft categories

* Minimum aircraft equipment
    * airspeed indicator
    * altimeter, adjustable in millibars
    * direct or remote reading compass
    * accurate timepiece
    * turn and slip indicator
    * OAT where ambient temp unavailable from ground instruments
    * attitude indicator
    * heading indicator
    * indication of power supply to gyroscopic instruments
    * landing light
    * illumination of all instruments
    * lights in all passenger compartments
    * torch per crew member

----

### FIR-TE9; FIR-TE9.3 Review – Underpinning knowledge required for units IFF, IFL, NVR1, NVR2 and NVR3 as applicable

* IFF Instrument flight full panel
* IFL Limited instrument panel manoeuvres
* NVR1 Conduct a traffic pattern at night
* NVR2 Night VFR – single-engine aircraft
* NVR3 Not applicable

----

### Part 61T & FIR-TE9.1(c); FIR-TE9.8;FIR7.3(b) Briefing - Privileges and limitations of the night VFR rating training endorsement. Administration procedures and responsibilities including issue of endorsements

----

### FIR-TE9.4 Tutorials 2 & 3 – Preparing lesson plans and pre-flight briefs for night VFR rating training lessons
