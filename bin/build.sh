#!/usr/bin/env bash

set -e
set -x

CI_PAGES_DOMAIN=${1}
CI_PAGES_URL=${2}
CI_PROJECT_TITLE=${3}
CI_PROJECT_URL=${4}
COMMIT_TIME=${5}
GITLAB_USER_NAME=${6}
GITLAB_USER_EMAIL=${7}
CI_COMMIT_SHA=${8}
CI_PROJECT_VISIBILITY=${9}

if ! type -p xelatex >/dev/null ; then
  >&2 echo "Please install xelatex" >&2
  exit 127
fi

if ! type -p pandoc >/dev/null ; then
  >&2 echo "Please install pandoc" >&2
  exit 127
fi

if ! type -p gs >/dev/null ; then
  >&2 echo "Please install ghostscript" >&2
  exit 127
fi

if ! type -p convert >/dev/null ; then
  >&2 echo "Please install imagemagick" >&2
  exit 127
fi

if ! type -p libreoffice >/dev/null ; then
  >&2 echo "Please install libreoffice" >&2
  exit 127
fi

if ! type -p rsync >/dev/null ; then
  >&2 echo "Please install rsync" >&2
  exit 127
fi

script_dir=$(dirname "$0")
dist_dir=${script_dir}/../public
src_dir=${script_dir}/../src
share_dir=${script_dir}/../share

mkdir -p ${dist_dir}

function makePage1 () {
  pdf_file=${1}
  pdf_basename=$(basename -- "${pdf_file}")
  pdf_dirname=$(dirname -- "${pdf_file}")
  pdf_filename="${pdf_basename%.*}"

  page1_pdf_dir=${pdf_dirname}
  page1_pdf="${page1_pdf_dir}/${pdf_filename}_page1.pdf"

  mkdir -p "${page1_pdf_dir}"

  gs -q -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER -dFirstPage=1 -dLastPage=1 -sOutputFile=${page1_pdf} ${pdf_file}

  page1_pdf_png="${page1_pdf}.png"
  convert ${page1_pdf} ${page1_pdf_png}
}

function preProcess() {
  # pre-process files
  pre_src_files=$(find -L ${src_dir} -type f | sort)

  for pre_src_file in ${pre_src_files}; do
    pre_src_file_relative=$(realpath --relative-to=${src_dir} ${pre_src_file})
    pre_src_file_relative_dirname=$(dirname ${pre_src_file_relative})
    pre_src_file_basename=$(basename -- "${pre_src_file_relative}")
    pre_src_file_extension="${pre_src_file_relative##*.}"

    dist_dir_relative=${dist_dir}/${pre_src_file_relative_dirname}
    mkdir -p ${dist_dir_relative}

    if [ ${pre_src_file_extension} = "fodt" ] || \
      [ ${pre_src_file_extension} = "fodg" ] || \
      [ ${pre_src_file_extension} = "txt"  ] || \
      [ ${pre_src_file_extension} = "html" ]; then
      sed -e "s#\${CI_PAGES_DOMAIN}#${CI_PAGES_DOMAIN}#g" \
          -e "s#\${CI_PAGES_URL}#${CI_PAGES_URL}#g" \
          -e "s#\${CI_PROJECT_TITLE}#${CI_PROJECT_TITLE}#g" \
          -e "s#\${CI_PROJECT_URL}#${CI_PROJECT_URL}#g" \
          -e "s#\${COMMIT_TIME}#${COMMIT_TIME}#g" \
          -e "s#\${GITLAB_USER_NAME}#${GITLAB_USER_NAME}#g" \
          -e "s#\${GITLAB_USER_EMAIL}#${GITLAB_USER_EMAIL}#g" \
          -e "s#\${CI_COMMIT_SHA}#${CI_COMMIT_SHA}#g" \
          -e "s#\${CI_PROJECT_VISIBILITY}#${CI_PROJECT_VISIBILITY}#g" \
          ${pre_src_file} > ${dist_dir_relative}/${pre_src_file_basename}
    else if [ ${pre_src_file_extension} = "md" ]; then
      (echo -e "# ${CI_PROJECT_TITLE}" && \
      echo -e "\n----\n" && \
      cat ${pre_src_file} && \
      echo -e "\n----\n" && \
      echo -e "### This document\n\n" && \
      echo -e "* hosted at **[${CI_PAGES_URL}](${CI_PAGES_URL})**\n" && \
      echo -e "* source hosted at **[${CI_PROJECT_URL}](${CI_PROJECT_URL})**\n" && \
      echo -e "* last updated at time **${COMMIT_TIME}**\n" && \
      echo -e "* last updated by user **[${GITLAB_USER_NAME}](mailto:${GITLAB_USER_EMAIL})**\n" && \
      echo -e "* revision **[${CI_COMMIT_SHA}](${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA})**\n" && \
      echo -e "* access control **${CI_PROJECT_VISIBILITY}**\n" && \
      echo -e "\n----\n" && \
      cat ${share_dir}/licence.md &&\
      echo -e "\n----\n"
      ) > ${dist_dir_relative}/${pre_src_file_basename}
    else
      rsync -aH ${pre_src_file} ${dist_dir_relative}/${pre_src_file_basename}
    fi
    fi
  done
}


function libreofficeFiles() {
  # libreoffice
  libre_files=$(find -L ${dist_dir} -name '*.fodg' -o -name '*.fodt' -o -name '*.odg' -o -name '*.odt' -type f | sort)

  for libre_file in ${libre_files}; do
    libre_file_relative=$(realpath --relative-to=${dist_dir} ${libre_file})
    libre_file_relative_dirname=$(dirname ${libre_file_relative})
    libre_file_basename=$(basename -- "${libre_file_relative}")
    libre_file_extension="${libre_file_relative##*.}"

    dist_dir_relative=${dist_dir}/${libre_file_relative_dirname}
    mkdir -p ${dist_dir_relative}

    for format in pdf html; do
      libreoffice --invisible --headless --convert-to ${format} ${libre_file} --outdir ${dist_dir_relative}
    done
  done
}

function markdownFiles() {
  # markdown
  md_files=$(find -L ${dist_dir} -name '*.md' -type f | sort)

  for md_file in ${md_files}; do
    md_file_relative=$(realpath --relative-to=${dist_dir} ${md_file})
    md_file_relative_dirname=$(dirname ${md_file_relative})
    md_file_basename=$(basename -- "${md_file}")
    md_file_filename="${md_file_basename%.*}"

    dist_dir_relative=${dist_dir}/${md_file_relative_dirname}
    mkdir -p ${dist_dir_relative}

    for format in pdf html docx odt; do
      pandoc -V geometry:margin=16.0mm -fmarkdown-implicit_figures -M mainfont="DejaVu Sans Mono" --pdf-engine=xelatex ${md_file} -o ${dist_dir_relative}/${md_file_filename}.${format}
    done
  done
}

function maat() {
  if  [ -f ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf ] && \
      [ -f ${dist_dir}/maat/02-FIR-TE9_Night_VFR_TE_-_Planning_matrix-1.pdf ] && \
      [ -f ${dist_dir}/maat/03-Night_VFR_TE9-01_\&_TE9-02_-_Review_NVFR_MOS_and_CBT_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/04-Night_VFR_TE9-03_-_Review_NVFR_MOS_underpinning_knowledge_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/05-Night_VFR_TE9-04_-_Privileges_and_limitations_of_night_VFR_TE_-_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/06-Night_VFR_TE9-05_-_Tutorials_2_\&_3_prepare_lesson_plan_\&_conduct_a_pre-flight_brief_-_training_plan.pdf ] && \
      [ -f ${dist_dir}/maat/07-Night_VFR_TE9-06_-_Aircraft_handling_from_instructor_control_seat_-_pre-flight_in-flight_training.pdf ] && \
      [ -f ${dist_dir}/maat/08-Night_VFR_TE9-07_and_9-08_-_BIF_long_brief_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/09-Night_VFR_TE9-10_-_BIF_-_IFF_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/10-Night_VFR_TE9-11_-_BIF_-_IFL_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/11-Night_VFR_TE9-12_and_9-13_-_Circuits_long_brief_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/12-Night_VFR_TE9-14_-_Ngt_Cts_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/13-Night_VFR_TE9-15_-_Ngt_Cts_consolidation_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/14-Night_VFR_TE9-16_-_Ngt_Cts_emergencies_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/15-Night_VFR_TE9-17_and_9-18_-_Night_navigation_long_brief_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/16-Night_VFR_TE9-19_-_Navigation_Aid_training_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/17-Night_VFR_TE9-20_-_Night_navigation_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf ] && \
      [ -f ${dist_dir}/maat/18-Night_VFR_TE9-21_-_Night_VFR_flight_review_lesson_plan_\&_training_record.pdf ]; then
    pdftk \
      ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf \
      ${dist_dir}/maat/02-FIR-TE9_Night_VFR_TE_-_Planning_matrix-1.pdf \
      ${dist_dir}/maat/03-Night_VFR_TE9-01_\&_TE9-02_-_Review_NVFR_MOS_and_CBT_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/04-Night_VFR_TE9-03_-_Review_NVFR_MOS_underpinning_knowledge_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/05-Night_VFR_TE9-04_-_Privileges_and_limitations_of_night_VFR_TE_-_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/06-Night_VFR_TE9-05_-_Tutorials_2_\&_3_prepare_lesson_plan_\&_conduct_a_pre-flight_brief_-_training_plan.pdf \
      ${dist_dir}/maat/07-Night_VFR_TE9-06_-_Aircraft_handling_from_instructor_control_seat_-_pre-flight_in-flight_training.pdf \
      ${dist_dir}/maat/08-Night_VFR_TE9-07_and_9-08_-_BIF_long_brief_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/09-Night_VFR_TE9-10_-_BIF_-_IFF_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/10-Night_VFR_TE9-11_-_BIF_-_IFL_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/11-Night_VFR_TE9-12_and_9-13_-_Circuits_long_brief_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/12-Night_VFR_TE9-14_-_Ngt_Cts_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/13-Night_VFR_TE9-15_-_Ngt_Cts_consolidation_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/14-Night_VFR_TE9-16_-_Ngt_Cts_emergencies_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/15-Night_VFR_TE9-17_and_9-18_-_Night_navigation_long_brief_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/16-Night_VFR_TE9-19_-_Navigation_Aid_training_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/17-Night_VFR_TE9-20_-_Night_navigation_pre-flight_\&_in-flight_lesson_plan_\&_training_record.pdf \
      ${dist_dir}/maat/18-Night_VFR_TE9-21_-_Night_VFR_flight_review_lesson_plan_\&_training_record.pdf \
      cat output \
      ${dist_dir}/maat.pdf
  else
    >&2 echo "Missing files (MAAT)" >&2
    exit 127
  fi
}

function booklet() {
  if [ -f ${dist_dir}/booklet-contents.pdf ] && \
     [ -f ${dist_dir}/casr1998-part61-subpart61o.pdf ] && \
     [ -f ${dist_dir}/casr1998-part61-manual-of-standards_volume2-schedule2-nvfr-competency-standards.pdf ] && \
     [ -f ${dist_dir}/casr1998-part61-manual-of-standards_volume2-schedule2-radio-navigation-competency-standards.pdf ] && \
     [ -f ${dist_dir}/casr1998-part61-manual-of-standards-volume3_appendix-2_night-vfr-rating.pdf ] && \
     [ -f ${dist_dir}/casr1998-part61-manual-of-standards-volume4_schedule5-section-o-night-vfr-rating.pdf ] && \
     [ -f ${dist_dir}/casr1998-part61-manual-of-standards_volume4-schedule8-tolerances-competency-standards.pdf ] && \
     [ -f ${dist_dir}/part91-mos-26.07.pdf ] && \
     [ -f ${dist_dir}/part91-mos-26.7.pdf ] && \
     [ -f ${dist_dir}/aip-enr-1.1-alternate-aerodromes.pdf ] && \
     [ -f ${dist_dir}/aip-enr-1.1-suitability-of-aerodromes.pdf ] && \
     [ -f ${dist_dir}/aip-gen-3.3-lsalt.pdf ] && \
     [ -f ${dist_dir}/vfrg-night-vfr.pdf ]; then
    pdftk \
      ${dist_dir}/booklet-contents.pdf \
      ${dist_dir}/casr1998-part61-subpart61o.pdf \
      ${dist_dir}/casr1998-part61-manual-of-standards_volume2-schedule2-nvfr-competency-standards.pdf \
      ${dist_dir}/casr1998-part61-manual-of-standards_volume2-schedule2-radio-navigation-competency-standards.pdf \
      ${dist_dir}/casr1998-part61-manual-of-standards-volume3_appendix-2_night-vfr-rating.pdf \
      ${dist_dir}/casr1998-part61-manual-of-standards-volume4_schedule5-section-o-night-vfr-rating.pdf \
      ${dist_dir}/casr1998-part61-manual-of-standards_volume4-schedule8-tolerances-competency-standards.pdf \
      ${dist_dir}/part91-mos-26.07.pdf \
      ${dist_dir}/part91-mos-26.7.pdf \
      ${dist_dir}/aip-enr-1.1-alternate-aerodromes.pdf \
      ${dist_dir}/aip-enr-1.1-suitability-of-aerodromes.pdf \
      ${dist_dir}/aip-gen-3.3-lsalt.pdf \
      ${dist_dir}/vfrg-night-vfr.pdf \
      cat output \
      ${dist_dir}/booklet.pdf
  else
    >&2 echo "booklet files missing" >&2
    exit 127
  fi
}

function groundTrainingSummary() {
  if  [ -f ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf ]; then
    pdftk \
      ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf \
      cat \
      10-11 \
      output \
      ${dist_dir}/maat/ground-and-flight-training-summary.pdf
  else
    >&2 echo "Missing files (MAAT 01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf)" >&2
    exit 127
  fi
}

function progressAchievementRecord() {
  if  [ -f ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf ]; then
    pdftk \
      ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf \
      cat \
      12-13 \
      output \
      ${dist_dir}/maat/progress-and-achievement-record.pdf
  else
    >&2 echo "Missing files (MAAT 01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf)" >&2
    exit 127
  fi
}

function traineeCompetencyRecord() {
  if  [ -f ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf ]; then
    pdftk \
      ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf \
      cat \
      14 \
      output \
      ${dist_dir}/maat/trainee-competency-record.pdf
  else
    >&2 echo "Missing files (MAAT 01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf)" >&2
    exit 127
  fi
}

function courseCompletion() {
  if  [ -f ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf ]; then
    pdftk \
      ${dist_dir}/maat/01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf \
      cat \
      15 \
      output \
      ${dist_dir}/maat/course-completion-certificate.pdf
  else
    >&2 echo "Missing files (MAAT 01-FIR-TE9_Night_VFR_training_endorsement_v1.0-1.pdf)" >&2
    exit 127
  fi
}

function nvfrQuestionsAndReferences() {
  if  [ -f ${dist_dir}/night-vfr-questions-and-references-cover.pdf ] && \
      [ -f ${dist_dir}/night-vfr-questions.pdf ] && \
      [ -f ${dist_dir}/casr1998-part61-subpart61o.pdf ] && \
      [ -f ${dist_dir}/part91-mos-26.7.pdf ] && \
      [ -f ${dist_dir}/part91-mos-26.07.pdf ] && \
      [ -f ${dist_dir}/aip-gen-3.3-lsalt.pdf ] && \
      [ -f ${dist_dir}/aip-enr-1.1-alternate-aerodromes.pdf ] && \
      [ -f ${dist_dir}/aip-enr-1.1-suitability-of-aerodromes.pdf ] && \
      [ -f ${dist_dir}/casr1998-91.740.pdf ]; then
    pdftk \
      ${dist_dir}/night-vfr-questions-and-references-cover.pdf \
      ${dist_dir}/night-vfr-questions.pdf \
      ${dist_dir}/casr1998-part61-subpart61o.pdf \
      ${dist_dir}/part91-mos-26.7.pdf \
      ${dist_dir}/part91-mos-26.07.pdf \
      ${dist_dir}/aip-gen-3.3-lsalt.pdf \
      ${dist_dir}/aip-enr-1.1-alternate-aerodromes.pdf \
      ${dist_dir}/aip-enr-1.1-suitability-of-aerodromes.pdf \
      ${dist_dir}/casr1998-91.740.pdf \
      cat output \
      ${dist_dir}/night-vfr-questions-and-references.pdf
  else
    >&2 echo "Missing files (Night VFR Questions & References)" >&2
    exit 127
  fi
}

function pdf() {
  # page 1
  pdf_files=$(find -L ${dist_dir} -name '*.pdf' -type f | sort)

  for pdf_file in ${pdf_files}; do
    pdf_file_relative=$(realpath --relative-to=${dist_dir} ${pdf_file})
    pdf_file_relative_dirname=$(dirname ${pdf_file_relative})
    pdf_file_basename=$(basename -- "${pdf_file}")
    pdf_file_filename="${pdf_file_basename%.*}"

    echo ${pdf_file_relative}
    echo ${pdf_file_relative_dirname}

    dist_dir_relative=${dist_dir}/${pdf_file_relative_dirname}
    mkdir -p ${dist_dir_relative}

    makePage1 ${dist_dir_relative}/${pdf_file_filename}.pdf
  done
}

function png() {
  # page 1
  png_files=$(find -L ${dist_dir} -name '*.png' -type f | sort)

  for png_file in ${png_files}; do
    for size in 600 150
    do
      page1_png_size="${png_file}-${size}.png"
      convert ${png_file} -resize ${size}x${size} ${page1_png_size}
    done
  done
}

preProcess
libreofficeFiles
rsync -aH ${share_dir}/ ${dist_dir}
markdownFiles
maat
booklet
groundTrainingSummary
progressAchievementRecord
traineeCompetencyRecord
courseCompletion
nvfrQuestionsAndReferences
pdf
png
